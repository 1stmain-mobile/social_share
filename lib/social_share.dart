import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';

class SocialShare {
  static const MethodChannel _channel = const MethodChannel('social_share');

  static Future<Map> checkInstalledAppsForShare() async =>
      await _channel.invokeMethod('checkInstalledApps');

  static Future shareLinkedIn(String caption, String attributionURL) async {
    Map<String, dynamic> args = <String, dynamic>{
      "caption": caption,
      "attributionURL": attributionURL,
    };
    return await _channel.invokeMethod('shareLinkedIn', args);
  }

  static Future shareFacebook(String caption, String attributionURL) async {
    Map<String, dynamic> args = <String, dynamic>{
      "caption": caption,
      "attributionURL": attributionURL,
    };
    return await _channel.invokeMethod('shareFacebook', args);
  }

  static Future shareTwitter(String captionText,
      {List<String> hashtags, String url, String trailingText = ""}) async {
    Map<String, dynamic> args;
    String modifiedUrl;
    if (Platform.isAndroid) {
      modifiedUrl = Uri.parse(url).toString().replaceAll('#', "%23");
    } else {
      modifiedUrl = Uri.parse(url).toString();
    }
    if (hashtags != null && hashtags.isNotEmpty) {
      String tags = "";
      hashtags.forEach((f) {
        tags += ("%23" + f.toString() + " ").toString();
      });
      args = <String, dynamic>{
        "caption":
            Uri.parse(captionText + "\n" + tags.toString()).toString(),
        "attributionURL": modifiedUrl,
        "trailingText": Uri.parse(trailingText).toString()
      };
    } else {
      args = <String, dynamic>{
        "caption": Uri.parse(captionText + " ").toString(),
        "attributionURL": modifiedUrl,
        "trailingText": Uri.parse(trailingText).toString()
      };
    }
    return await _channel.invokeMethod('shareTwitter', args);
  }

  static Future shareWhatsAppText(String caption, String subject) async {
    Map<String, dynamic> args = <String, dynamic>{
      "caption": caption,
      "subject": subject,
    };
    return await _channel.invokeMethod('shareWhatsappText', args);
  }

  static Future shareTelegram(String content) async {
    final Map<String, dynamic> args = <String, dynamic>{"caption": content};
    return await _channel.invokeMethod('shareTelegram', args);
  }

  static Future shareMessenger(String caption, String attributionURL) async {
    Map<String, dynamic> args = <String, dynamic>{
      "caption": caption,
      "attributionURL": attributionURL,
    };
    return await _channel.invokeMethod('shareMessenger', args);
  }
}
