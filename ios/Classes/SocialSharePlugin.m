#import "SocialSharePlugin.h"
#include <objc/runtime.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@implementation SocialSharePlugin {
    FlutterMethodChannel* _channel;
    UIDocumentInteractionController* _dic;
    FlutterResult _result;
}

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    FlutterMethodChannel* channel = [FlutterMethodChannel
                                     methodChannelWithName:@"social_share"
                                     binaryMessenger:[registrar messenger]];
    SocialSharePlugin* instance = [[SocialSharePlugin alloc] initWithChannel:channel];
    [registrar addApplicationDelegate:instance];
    [registrar addMethodCallDelegate:instance channel:channel];
}

- (instancetype)initWithChannel:(FlutterMethodChannel*)channel {
    self = [super init];
    if(self) {
        _channel = channel;
    }
    return self;
}

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:
(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    return [[FBSDKApplicationDelegate sharedInstance]
            application:application
            openURL:url
            sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
            annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    BOOL handled =
    [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url
                                         sourceApplication:sourceApplication
                                                annotation:annotation];
    return handled;
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    _result = result;
    if ([@"shareFacebook" isEqualToString:call.method]) {
        NSURL *fbURL = [NSURL URLWithString:@"fbapi://"];
        if([[UIApplication sharedApplication] canOpenURL:fbURL]) {
            [self facebookShare:call.arguments[@"attributionURL"]];
            result(@"sharing");
        } else {
            NSString *fbLink = @"itms-apps://itunes.apple.com/us/app/apple-store/id284882215";
            if (@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fbLink] options:@{} completionHandler:nil];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fbLink] options:@{} completionHandler:nil];
            }
            result(@"not supported or no facebook installed");
        }
    }else if([@"shareTwitter" isEqualToString:call.method]){
        NSString *captionText = call.arguments[@"caption"];
        NSString *urlstring = call.arguments[@"attributionURL"];
        NSString *trailingText = call.arguments[@"trailingText"];
        
        NSString* urlTextEscaped = [urlstring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString: urlTextEscaped];
        NSURL *urlScheme = [NSURL URLWithString:@"twitter://"];
        if ([[UIApplication sharedApplication] canOpenURL:urlScheme]) {
            
            if ( [ [url absoluteString]  length] == 0 ){
                NSString *urlSchemeTwitter = [NSString stringWithFormat:@"twitter://post?message=%@",captionText];
                NSURL *urlSchemeSend = [NSURL URLWithString:urlSchemeTwitter];
                if (@available(iOS 10.0, *)) {
                    [[UIApplication sharedApplication] openURL:urlSchemeSend options:@{} completionHandler:nil];
                    result(@"sharing");
                } else {
                    result(@"this only supports iOS 10+");
                }
            }else{
                //check if trailing text equals null
                if ( [ trailingText   length] == 0 ){
                    //if trailing text is null
                    NSString *urlSchemeSms = [NSString stringWithFormat:@"twitter://post?message=%@",captionText];
                    //appending url with normal text and url scheme
                    NSString *urlWithLink = [urlSchemeSms stringByAppendingString:[url absoluteString]];
                    
                    //final urlscheme
                    NSURL *urlSchemeMsg = [NSURL URLWithString:urlWithLink];
                    if (@available(iOS 10.0, *)) {
                        [[UIApplication sharedApplication] openURL:urlSchemeMsg options:@{} completionHandler:nil];
                        result(@"sharing");
                    } else {
                        result(@"this only supports iOS 10+");
                    }
                }else{
                    //if trailing text is not null
                    NSString *urlSchemeSms = [NSString stringWithFormat:@"twitter://post?message=%@",captionText];
                    //appending url with normal text and url scheme
                    NSString *urlWithLink = [urlSchemeSms stringByAppendingString:[url absoluteString]];
                    NSString *finalurl = [urlWithLink stringByAppendingString:trailingText];
                    //final urlscheme
                    NSURL *urlSchemeMsg = [NSURL URLWithString:finalurl];
                    if (@available(iOS 10.0, *)) {
                        [[UIApplication sharedApplication] openURL:urlSchemeMsg options:@{} completionHandler:nil];
                        result(@"sharing");
                    } else {
                        result(@"this only supports iOS 10+");
                    }
                }
            }
            
        }else{
            
            NSString *twitterLink = @"itms-apps://itunes.apple.com/us/app/apple-store/id333903271";
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:twitterLink] options:@{} completionHandler:nil];
            
            result(@"cannot find Twitter app");
        }
    }else if([@"shareLinkedIn" isEqualToString:call.method]){
        NSString *attributionUrl = call.arguments[@"attributionURL"];
        NSString *caption = call.arguments[@"caption"];
        NSString *browserUrlScheme = [NSString stringWithFormat:@"linkedin://shareArticle?mini=true&url=%@", attributionUrl];
        
        if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString: browserUrlScheme]]) {
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: browserUrlScheme] options:@{} completionHandler:nil];
            result(@"open LinkedIn on browser");
        } else {
            NSString *linkedInLink = @"itms-apps://itunes.apple.com/us/app/apple-store/id288429040";
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkedInLink] options:@{} completionHandler:nil];
            result(@"cannot open LinkedIn");
        }
    }else if([@"shareWhatsappText" isEqualToString:call.method]){
        NSString *subject = call.arguments[@"subject"];
        NSString *caption = call.arguments[@"caption"];
        NSString * urlScheme = [NSString stringWithFormat:@"whatsapp://send?text=%@",caption];
        NSURL *whatsappURL = [NSURL URLWithString:[urlScheme stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) { [[UIApplication sharedApplication] openURL: whatsappURL options:@{} completionHandler:nil];
            result(@"sharing");
        }else {
            result(@"cannot open Whatsapp");
        }
    }else if([@"shareTelegram" isEqualToString:call.method]){
        NSString *content = call.arguments[@"caption"];
        NSString * urlScheme = [NSString stringWithFormat:@"tg://msg?text=%@",content];
        NSURL * telegramURL = [NSURL URLWithString:[urlScheme stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        if ([[UIApplication sharedApplication] canOpenURL: telegramURL]) {
            [[UIApplication sharedApplication] openURL: telegramURL options:@{} completionHandler:nil];
            result(@"sharing");
        } else {
            result(@"cannot open Telegram");
        }
        result([NSNumber numberWithBool:YES]);
    }
    else if([@"shareMessenger" isEqualToString:call.method]){
        NSString *caption = call.arguments[@"caption"];
        NSString *attributionURL = call.arguments[@"attributionURL"];
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        
        content.quote = caption;
        content.contentURL = [NSURL URLWithString:attributionURL];
        if ([[[FBSDKMessageDialog alloc] init] canShow]) {
            [FBSDKMessageDialog showWithContent:content delegate:self];
            result(@"sharing");
        }
        else {
            NSString *fbLink = @"itms-apps://itunes.apple.com/us/app/apple-store/id454638411";
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fbLink] options:@{} completionHandler:nil];
            
            result(@"not supported or no facebook messenger installed");
        }
    }
    else if([@"checkInstalledApps" isEqualToString:call.method]){
        NSMutableDictionary *installedApps = [[NSMutableDictionary alloc] init];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"linkedin://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"linkedin"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"linkedin"];
        }
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"facebook-stories://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"facebook"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"facebook"];
        }
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"twitter"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"twitter"];
        }
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"whatsapp://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"whatsapp"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"whatsapp"];
        }
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb-messenger-share-api://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"messenger"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"messenger"];
        }
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tg://"]]) {
            [installedApps setObject:[NSNumber numberWithBool: YES] forKey:@"telegram"];
        }else{
            [installedApps setObject:[NSNumber numberWithBool: NO] forKey:@"telegram"];
        }
        result(installedApps);
    }
    else {
        result(FlutterMethodNotImplemented);
    }
}

- (void)facebookShare:(NSString*)attributionURL {
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:attributionURL];
    UIViewController* controller = [UIApplication sharedApplication].delegate.window.rootViewController;
    [FBSDKShareDialog showFromViewController:controller withContent:content delegate:self];
}

-(UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{
    [_channel invokeMethod:@"onSuccess" arguments:nil];
    NSLog(@"Sharing completed successfully");
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer{
    [_channel invokeMethod:@"onCancel" arguments:nil];
    NSLog(@"Sharing cancelled");
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{
    [_channel invokeMethod:@"onError" arguments:nil];
    NSLog(@"%@",error);
}

@end
