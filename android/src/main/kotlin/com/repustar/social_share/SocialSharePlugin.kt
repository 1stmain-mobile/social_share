package com.repustar.social_share

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar


class SocialSharePlugin(private val registrar: Registrar) : MethodCallHandler {

    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val channel = MethodChannel(registrar.messenger(), "social_share")
            channel.setMethodCallHandler(SocialSharePlugin(registrar))
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        val activity: Activity = registrar.activity()
        val context: Context = registrar.activeContext()

        if (call.method == "shareFacebook") {
            //share on facebook feed
            val caption: String? = call.argument("caption")
            val attributionURL: String? = call.argument("attributionURL")

            val content = ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(attributionURL))
                    .build()
            val shareDialog = ShareDialog(activity)
            if (ShareDialog.canShow(ShareLinkContent::class.java)) {
                result.success(true)
                shareDialog.show(content)
            } else {
                val pm: PackageManager = context.packageManager
                val packages = pm.getInstalledApplications(PackageManager.GET_META_DATA)
                if (!packages.any {
                            it.packageName.toString().contentEquals("com.facebook.katana")
                        })
                    openToAppPage("com.facebook.katana", context)
                result.success(false)
            }
        } else if (call.method == "shareLinkedIn") {
            val text: String? = call.argument("caption")
            val url: String? = call.argument("attributionURL")
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/*"
            shareIntent.putExtra(Intent.EXTRA_TEXT, url)
            shareIntent.setPackage("com.linkedin.android");
            if (activity.packageManager.resolveActivity(shareIntent, 0) != null) {
                result.success(true)
                activity.startActivity(shareIntent)
            } else {
                openToAppPage("com.linkedin.android", context)
                result.success(false)
            }
        } else if (call.method == "shareWhatsappText") {
            //shares content on WhatsApp
            val subject: String? = call.argument("subject")
            val caption: String? = call.argument("caption")

            val whatsappIntent = Intent(Intent.ACTION_SEND)
            whatsappIntent.type = "text/plain"
            whatsappIntent.setPackage("com.whatsapp")
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, caption)
            if (activity.packageManager.resolveActivity(whatsappIntent, 0) != null) {
                result.success(true)
                activity.startActivity(whatsappIntent)
            } else {
                openToAppPage("com.whatsapp", context)
                result.success(false)
            }
        } else if (call.method == "shareTwitter") {
            //shares content on twitter
            val text: String? = call.argument("caption")
            val url: String? = call.argument("attributionURL")
            val trailingText: String? = call.argument("trailingText")
            val urlScheme = "http://www.twitter.com/intent/tweet?text=$text$url$trailingText"
            Log.d("log", urlScheme)
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(urlScheme)
            if (activity.packageManager.resolveActivity(intent, 0) != null) {
                result.success(true)
                activity.startActivity(intent)
            } else {
                openToAppPage("com.twitter.android", context)
                result.success(false)
            }
        } else if (call.method == "shareTelegram") {
            //shares content on Telegram
            val content: String? = call.argument("caption")
            val telegramIntent = Intent(Intent.ACTION_SEND)
            telegramIntent.type = "text/plain"
            telegramIntent.setPackage("org.telegram.messenger")
            telegramIntent.putExtra(Intent.EXTRA_TEXT, content)
            if (activity.packageManager.resolveActivity(telegramIntent, 0) != null) {
                result.success(true)
                activity.startActivity(telegramIntent)
            } else {
                openToAppPage("org.telegram.messenger", context)
                result.success(false)
            }
        } else if (call.method == "checkInstalledApps") {
            //check if the apps exists
            //creating a mutable map of apps
            val apps: MutableMap<String, Boolean> = mutableMapOf()
            //assigning package manager
            val pm: PackageManager = context.packageManager
            //get a list of installed apps.
            val packages = pm.getInstalledApplications(PackageManager.GET_META_DATA)
            //if other app exists
            apps["instagram"] = packages.any { it.packageName.toString().contentEquals("com.facebook.katana") }
            apps["facebook"] = packages.any { it.packageName.toString().contentEquals("com.facebook.katana") }
            apps["twitter"] = packages.any { it.packageName.toString().contentEquals("com.twitter.android") }
            apps["whatsapp"] = packages.any { it.packageName.toString().contentEquals("com.whatsapp") }
            apps["messenger"] = packages.any { it.packageName.toString().contentEquals("com.facebook.orca") }
            apps["telegram"] = packages.any { it.packageName.toString().contentEquals("org.telegram.messenger") }
            result.success(apps)
        } else {
            result.notImplemented()
        }
    }

    fun openToAppPage(packageId: String, context: Context) {
        val url = "https://play.google.com/store/apps/details?id=" + packageId
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        context.startActivity(intent)
    }
}
